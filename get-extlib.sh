#!/bin/sh

echo "get ext lib for gstreamer"
CUR_DIR=`pwd`
EXT_DIR=${CUR_DIR}/../ext

if [ -d ${EXT_DIR} ] ; then
	echo "ext is exit"
else
	mkdir ${EXT_DIR}
fi

while read line
do
	echo ${line}
	cd ${EXT_DIR}
	${line}
	cd ${CUR_DIR}
done < extlib.txt
