#!/bin/sh

#set -x

GST_URL="git://anongit.freedesktop.org/gstreamer"
GET="get"
BUILD="build"
CUR_DIR=`pwd`

echo "please input youe choose"
echo "1------get source"
echo "2------build gst all"
echo "3------build ext lib"
echo "4------update source"
echo ""

read INPUT
echo "your choose is ${INPUT}"

case ${INPUT} in
	1)
	for PROJECT_NAME in $(cat gst-list.txt) ; do
		echo "dir is ${PROJECT_NAME}"
		if [ -d ../${PROJECT_NAME} ]; then
			cd ../${PROJECT_NAME}
			git pull
			cd ${CUR_DIR}
		else
			echo ${GST_URL}/${PROJECT_NAME}
 	       		git clone ${GST_URL}/${PROJECT_NAME} ../${PROJECT_NAME}
		fi
	done
	;;
	
	2)
	for PROJECT_NAME in $(cat gst-list.txt) ; do
		echo "dir is ${PROJECT_NAME}"
		if [ -d ../${PROJECT_NAME} ]; then
			cd ../${PROJECT_NAME}
			./autogen.sh && ../configure --prefi=/usr && make && sudo make install 
			cd ${CUR_DIR}
			echo "compile ${PROJECT_NAME} success"
		else
                         git clone ${GST_URL}/${PROJECT_NAME} ../${PROJECT_NAME}
		fi
		 
		 cd ../${PROJECT_NAME}
                 ./autogen.sh && ../configure --prefi=/usr && make && sudo make install
                 cd ${CUR_DIR}
                 echo "compile ${PROJECT_NAME} success"
	done
	;;
	4)
	for PROJECT_NAME in $(cat gst-list.txt) ; do
                echo "dir is ${PROJECT_NAME}"
                if [ -d ../${PROJECT_NAME} ]; then
                        cd ../${PROJECT_NAME}
                        git pull
                        cd ${CUR_DIR}
                        echo "update ${PROJECT_NAME} source"
                fi
        done
        ;;


esac
	
