#!/bin/sh
echo "***************************************"
echo "********generate ctags"

VARS="cscope ctags cflow"

for var in ${VARS} ; do
	echo "***********test tools ${var}"
	${var} --version
	if [ $? -eq 0 ] ; then
		echo "****${var} is find"
	else
		echo "****please install ${var}"
		exit;
	fi
done

find ./ -name "*.h" -o -name "*.c" -o -name "*.C" -o -name "*.cpp" -o -name "*.hpp" > cscope.file
cscope -bkq -i cscope.file
ctags -R
