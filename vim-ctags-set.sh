#!/bin/sh

if [ ! -d ~/.vim ] ; then
	mkdir ~/.vim
fi

cp -rf vim/vim/*  ~/.vim/


while read line
do
	sudo echo $line > /etc/vim/vimrc
done < ./vim/vimrc.txt
