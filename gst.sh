#!/bin/sh

echo "1  get gst"
echo "2  get ext lib"
echo "3  build ext lib"
echo "4  build gstreamer"
echo "please input your choose:"

read CMD

case ${CMD} in
	1)
		echo "get gst ..."
		sh get-gst.sh
		;;
	2)
		echo "get extlib ..."
		sh get-extlib.sh
		;;
	3)
		echo "build ext lib ...."
		sh build-ext.sh
		;;
	4)
		echo "build gstreamer ...."
		sh build-gst.sh
		;;
esac
